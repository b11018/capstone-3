import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Login from './pages/Login';
import './App.css';
import { UserProvider } from './UserContext';
import NotFound from './pages/Error';
import AdminDashboard from './pages/AdminDashboard';
import Users from './pages/Users';
import UserOrderView from './pages/UserOrderView';
import CartView from './pages/CartView';

function App() {

	const [user,setUser] = useState({
		_id: null,
		isAdmin: null
	})

	const unsetUser = () => {
		localStorage.clear();
	}

	useEffect(() => {
		fetch('https://shielded-sea-38438.herokuapp.com/users/getUserDetails', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if(typeof data._id !== "undefined") {
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})

	}, [])

    return (
    	<UserProvider value={{user, setUser, unsetUser}}>
		    <Router>
		        <AppNavBar/>
		        <div className="font-link bgradient">
			        <Container className="container-fluid">
			        	<Routes>
			        	
				        	<Route exact path="/" element={<Home/>}/>
				        	<Route exact path="/products" element={<Products/>}/>
				        	<Route exact path="/productView/:productId" element={<ProductView/>}/>
				        	<Route exact path="/register" element={<Register/>}/>
				        	<Route exact path="/login" element={<Login/>}/>
				        	<Route exact path="/logout" element={<Logout/>}/>
				        	<Route exact path="/adminDashboard" element={<AdminDashboard/>}/>
				        	<Route exact path="/users" element={<Users/>}/>
				        	<Route exact path="/userOrderView/:customerId" element={<UserOrderView/>}/>
				        	<Route exact path="/cartView" element={<CartView/>}/>
				        	<Route path="*" element={<NotFound />} />

			        	</Routes>
			        </Container>
		        </div>
		    </Router>
	    </UserProvider>
    );
}

export default App;