import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function UserTable({userProp}) {
	// console.log(userProp)

	const {_id, firstName, lastName, email, orders} = userProp;

	return (
    	<>
        <tr className="styleProdTab">
          <td>{_id}</td>
          <td>{firstName} {lastName}</td>
          <td>{email}</td>
          <td>{orders.length}</td>
          <td className="alignCenter">
          	<Button className="btn-primary" as={ Link } to={`/userOrderView/${_id}`}>View Orders</Button>
          </td>
        </tr>
      </>
    )
}