import {Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner(){
	return (
		<Row>
			<Col className = "styleApp img-fluid p-5">
				<h1>The Guitar Shop</h1>
				<p>Electric Guitars in stock!</p>
				<Button className="btngradient" as={ Link } to="/products">Browse collection!</Button>
			</Col> 
		</Row>
	)
}