// import Table from 'react-bootstrap/Table';
import { Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
// MODAL IMPORTS
import React, { useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';

export default function ProductTable({productProp}) {
	// console.log(productProp);
	const {name, description, price, _id, inStock} = productProp;

	// MODAL USESTATE
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [newName, setNewName] = useState('');
	const [newDescription, setNewDescription] = useState('');
	const [newPrice, setNewPrice] = useState(0);

	const [isActive, setIsActive] = useState(false);
	const [prodUpdate, setProdUpdate] = useState(false);
	const [disabled, setDisabled] = useState(false);

	useEffect(() => {
		if(newName !== '' && newDescription !== '' && newPrice !== 0){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [newName, newDescription, newPrice]);

	// useEffect(() => {
	// 	console.log('prod updated, archived or activated')
	// }, [disabled, prodUpdate])

	const updateProduct = (e, productId) => {
		e.preventDefault();
		fetch(`https://shielded-sea-38438.herokuapp.com/products/updateProduct/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: newName,
				description: newDescription,
				price: newPrice
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.name !== name || data.description !== description || data.price !== price){
				Swal.fire({
					title: 'Product Update successful',
					icon: 'success',
					text: 'Modifications saved'
				})
				// setProdUpdate(true)
				// setProdUpdate(false)
				handleClose();
			} else {
				Swal.fire({
					title: 'Product Update failed',
					icon: 'error',
					text: 'Something went wrong'
				})
			}
		})
	}
	
	const archiveProduct = (e, productId) => {
		e.preventDefault();
		fetch(`https://shielded-sea-38438.herokuapp.com/products/archiveProduct/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				inStock: false
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: 'Deactivation Successful',
					icon: 'success',
					text: `${name} is no longer in stock`
				})
				// setDisabled(true);

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	}

	const activateProduct = (e, productId) => {
		fetch(`https://shielded-sea-38438.herokuapp.com/products/activateProduct/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				inStock: true
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: 'Activation Successful',
					icon: 'success',
					text: `${name} is now in stock`
				})
				// setDisabled(false);

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	}

    return (
    	<>
        <tr className="styleProdTab">
          <td>{name}</td>
          <td>{description}</td>
          <td className="alignCenter">${price}</td>
          <td className="alignCenter">{inStock ? `Available` : `No Stock`}</td>
          <td>
          	<div className="styleActionsBtn">
	          	<Button className="btn-primary" onClick={handleShow}>Update</Button>
	          	{inStock
	          		?
	          		<Button className="btn-danger" onClick={(e) => archiveProduct(e, _id)}>Disable</Button>
	          		:
	          		<Button className="btn-success" onClick={(e) => activateProduct(e, _id)}>Activate</Button>
	          	}
	        </div>
          </td>
        </tr>
        <>
          <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} centered>
          <Form onSubmit={ (e) => updateProduct(e, _id)}>
            <Modal.Header closeButton>
              <Modal.Title>Update Product Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder={name}
                    required
                    onChange={e => setNewName(e.target.value)}
                    autoFocus
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlTextarea1"
                >
                  <Form.Label>Description</Form.Label>
                  <Form.Control as="textarea" rows={3} 
                  placeholder={description}
                  required
                  onChange={e => setNewDescription(e.target.value)}
                  />
                </Form.Group>
                <Form.Group className=" mb-3" controlId="exampleForm.ControlInput2">
                  <Form.Label>Price</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={price}
                    required
                    onChange={e => setNewPrice(e.target.value)}
                  />
                </Form.Group>
              
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              {isActive
              	?
	              <Button variant="primary" type="submit">
	                Save Changes
	              </Button>
	              :
	              <Button variant="primary" disabled>
	                Save Changes
	              </Button>
	            }
            </Modal.Footer>
            </Form>
          </Modal>
        </>
      </>
    )
}