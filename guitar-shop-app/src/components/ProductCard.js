import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {
	// console.log(productProp);
	
	const {name, description, price, _id} = productProp;
	
    return (
        <Card className= "fc2 fsize1 mx-3 my-5 col-md-3 col-sm-12">
            <Card.Body className="bgc3">
                <Card.Header>
                    <Card.Title className="styleCardTitle m-3" >{name}</Card.Title>
                </Card.Header>
                    <Card.Subtitle className="mt-3">Specs:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle className="stylePrice">Price:${price}</Card.Subtitle>
                
            </Card.Body>
            <Card.Footer className="styleCardFooter bgradient d-grid gap-2">
                <Button as={Link} to={`/productView/${_id}`}>View Details</Button>
            </Card.Footer>
        </Card>
    )
}