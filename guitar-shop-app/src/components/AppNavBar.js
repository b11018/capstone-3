import { useContext } from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){
	
	const { user } = useContext(UserContext);

	return (
		<Navbar className="navbar active font-link fsize" expand="lg">
	      <Container className="fc1">
	        <Navbar.Brand as={ Link } to="/">The Guitar Shop</Navbar.Brand>
	        <Navbar.Toggle/>
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="ms-auto">
	            <Nav.Link as={ Link } to="/">Home</Nav.Link>
	            <Nav.Link as={ Link } to="/products">Collections</Nav.Link>
	            {user.isAdmin
		            ?
		            <Nav.Link as={ Link } to="/adminDashboard">Admin Dashboard</Nav.Link>
		            :
		            <Nav.Link as={ Link } to="/adminDashboard" hidden>Admin Dashboard</Nav.Link>
	            }
	            {(user.id !== null && user.isAdmin !== true)
	            	?
	            	<Nav.Link as={ Link } to="/cartView">Cart</Nav.Link>
	            	:
	            	<Nav.Link as={ Link } to="/cartView" hidden>Cart</Nav.Link>
	            }
	            {(user.id !== null)
		            ?
		            <>
		            
		            <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
		            </>
		            :
		            <>
		            <Nav.Link as={ Link } to="/login">Login</Nav.Link>
		            <Nav.Link as={ Link } to="/register">Register</Nav.Link>
		            </>
	        	}

	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}