import { Button, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function UserOrderTable({orderProp}) {
	console.log(orderProp)

	const {productName, productId, price, _id, inStock, quantity, subTotal} = orderProp;

	// console.log(_id)

	return (
    	<>
	        <tr className="styleProdTab">
	          <td>{productId}</td>
	          <td>{productName}</td>
	          <td>${price}</td>
	          <td>{quantity}</td>
	          <td>${subTotal}</td>
	        </tr>
	    </>
    )
}