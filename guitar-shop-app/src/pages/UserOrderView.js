import { useEffect, useState, useContext } from 'react';
import Table from 'react-bootstrap/Table';
import CartTable from '../components/CartTable';
import UserContext from '../UserContext';
import { Navigate, useParams } from 'react-router-dom';
import UserOrderTable from '../components/UserOrderTable';

export default function UserOrderView(){
	const {user} = useContext(UserContext);
	const {customerId} = useParams();
	const [customer, setCustomer] =useState([])
	const [orders, setOrders] =useState([])

	useEffect(() => {
		console.log(customerId)
		
		fetch(`https://shielded-sea-38438.herokuapp.com/users/getCustomerDetails/${customerId}`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setCustomer(`${data.firstName} ${data.lastName}`)
			setOrders(data.orders.map(order => {
				return (
					<UserOrderTable key={order._id} orderProp={order}/>
				)
			}))
		})
	}, [])
	
	return(
		(user.id !== null && user.isAdmin)
		?
		<>
			<h1 className="styleH1Admin">{customer}'s Orders</h1>
			
			<Table striped bordered hover responsive size="sm">
			      <thead className="table-dark">
			        <tr>
			          <th>Product Id</th>
			          <th>Product Name</th>
			          <th>Price</th>
			          <th>Quantity</th>
			          <th>Subtotal</th>
			        </tr>
			      </thead>
			      <tbody>
			      	{orders}
			      </tbody>
		    </Table>
		</>
		:
		<Navigate to="/products"/>
	)
}