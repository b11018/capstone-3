import UserTable from '../components/UserTable';
import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Table from 'react-bootstrap/Table';

export default function Users(){

	const {user} = useContext(UserContext);
	const [users, setUsers] =useState([])

	useEffect(() => {
		fetch('https://shielded-sea-38438.herokuapp.com/users/getAllUsers', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUsers(data.map(client => {
				return (
					<UserTable key={client._id} userProp={client}/>
				)
			}))
		})
	}, [])

	return(
		(user.id === null || user.isAdmin === false)
		?
		<Navigate to="/products"/>
		:
		<>
		<h1 className="styleH1Admin">User Orders</h1>
		<Table striped bordered hover responsive size="sm">
	      <thead className="table-dark">
	        <tr>
	          <th>User Id</th>
	          <th>Name</th>
	          <th>Email</th>
	          <th>Orders</th>
	          <th className="alignCenter">Actions</th>
	        </tr>
	      </thead>
	      <tbody>
	      	{users}
	      </tbody>
	    </Table>
	    </>
	)
}