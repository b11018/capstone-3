import ProductCard from '../components/ProductCard';
import { useEffect, useState } from 'react';

export default function Products(){

	const [products, setProducts] =useState([])

	useEffect(() => {
		fetch('https://shielded-sea-38438.herokuapp.com/products/getAllActiveProducts')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
				)
			}))
		})
	}, [])

	return(
		<>
			<h1 className="fc2 m-4"> Guitars In Stock:</h1>
			<div className="styleCard">
				{products}
			</div>
		</>
	)
}