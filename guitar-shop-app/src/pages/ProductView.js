import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

	const { user } = useContext(UserContext);

	const history = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	let [count, setCount] = useState(1);

	function incrementCount() {
	    count = count + 1;
	    setCount(count);
    }
    function decrementCount() {
        count = count - 1;
        setCount(count);
    }

	const addToCart = (productId) => {
		fetch('https://shielded-sea-38438.herokuapp.com/users/userCheckout', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: count,
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(productId);
			if(user.isAdmin){
				Swal.fire({
					title: 'Action Forbidden',
					icon: 'error',
					text: 'This is an Admin Account'
				})
			} else if(data){
				Swal.fire({
					title: 'Added Successfully',
					icon: 'success',
					text: 'View your items in cart!'
				})

				history("/products");
			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	}
	useEffect(() => {
		// console.log(productId)

		
		fetch(`https://shielded-sea-38438.herokuapp.com/products/getSingleproduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setName(data.name)
			setPrice(data.price)
			setDescription(data.description)
		})
		
	}, [])

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card className="fc2 fsize1">
						<Card.Body className="bgc3">
							<Card.Header>
							    <Card.Title className="styleCardTitle m-3" >{name}</Card.Title>
							</Card.Header>
							    <Card.Subtitle className="mt-3">Specs:</Card.Subtitle>
							    <Card.Text>{description}</Card.Text>
							    <Card.Subtitle>Price:</Card.Subtitle>
							    <Card.Text>${price}</Card.Text>
							    <Card.Text>Quantity:</Card.Text>
							    <div className="quantity">
							    	<button onClick={decrementCount}>-</button>
							        <div>{count}</div>
							        <button onClick={incrementCount}>+</button>
						        </div>

						</Card.Body>
						<Card.Footer className="bgradient">
							{ user.id !== null
								?
								<div className="d-grid gap-2">
									<Button onClick={() => addToCart(productId)}>Add to Cart</Button>
								</div>
								:
								<>
									<p>Not yet registered? <Link to="/register">Register Here</Link></p>
									<Link className="btn btn-danger" to="/login">Log In</Link>
								</>
							}
						</Card.Footer>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}