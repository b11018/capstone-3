import { Button, Form } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import ProductTable from '../components/ProductTable';
import { useEffect, useState, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Modal from 'react-bootstrap/Modal';

export default function AdminDashboard(){
	const {user} = useContext(UserContext);
	const [products, setProducts] =useState([])

	// MODAL USESTATE
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [newName, setNewName] = useState('');
	const [newDescription, setNewDescription] = useState('');
	const [newPrice, setNewPrice] = useState(0);

	const [isActive, setIsActive] = useState(false);

	// console.log(user.isAdmin)
	useEffect(() => {
		fetch('https://shielded-sea-38438.herokuapp.com/products/getAllActiveProducts')
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setProducts(data.map(product => {
				return (
					<ProductTable key={product._id} productProp={product}/>
				)
			}))
		})
		if(newName !== '' && newDescription !== '' && newPrice !== 0){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [newName, newDescription, newPrice, isActive, show]);
	

	const addProduct = (e, productId) => {
		e.preventDefault();
		fetch(`https://shielded-sea-38438.herokuapp.com/products/createProduct`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: newName,
				description: newDescription,
				price: newPrice
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.name !== ''){
				Swal.fire({
					title: 'Product Added successfully',
					icon: 'success',
					text: `${newName} is added to the collection`
				})
				handleClose();
			} else {
				Swal.fire({
					title: 'Product Addition failed',
					icon: 'error',
					text: 'Something went wrong'
				})
			}
		})
	}


	return(
		(user.id === null || user.isAdmin === false)
		?
		<Navigate to="/products"/>
		:
		<>
			<h1 className="styleH1Admin">Admin Dashboard</h1>
			<div className="styleBtnAdmin">
				<Button className="btn-primary" onClick={handleShow}>Add New Product</Button>
				<Button className="btn-success" as={ Link } to="/users">Show User Orders</Button>
			</div>
			<Table striped bordered hover responsive size="sm">
			      <thead className="table-dark">
			        <tr>
			          <th>Name</th>
			          <th>Description</th>
			          <th className="alignCenter">Price</th>
			          <th className="alignCenter">Availablity</th>
			          <th className="alignCenter">Actions</th>
			        </tr>
			      </thead>
			      <tbody>
			      	{products}
			      </tbody>
		    </Table>
	        <>
	          <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} centered>
	          <Form onSubmit={ (e) => addProduct(e)}>
	            <Modal.Header closeButton>
	              <Modal.Title>Add New Product</Modal.Title>
	            </Modal.Header>
	            <Modal.Body>
	              
	                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
	                  <Form.Label>Name</Form.Label>
	                  <Form.Control
	                    type="text"
	                    placeholder="New Product Name"
	                    required
	                    onChange={e => setNewName(e.target.value)}
	                    autoFocus
	                  />
	                </Form.Group>
	                <Form.Group
	                  className="mb-3"
	                  controlId="exampleForm.ControlTextarea1"
	                >
	                  <Form.Label>Description</Form.Label>
	                  <Form.Control as="textarea" rows={3} 
	                  placeholder="New Product Description"
	                  required
	                  onChange={e => setNewDescription(e.target.value)}
	                  />
	                </Form.Group>
	                <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
	                  <Form.Label>Price</Form.Label>
	                  <Form.Control
	                    type="number"
	                    placeholder="New Product Price"
	                    required
	                    onChange={e => setNewPrice(e.target.value)}
	                  />
	                </Form.Group>
	              
	            </Modal.Body>
	            <Modal.Footer>
	              <Button variant="secondary" onClick={handleClose}>
	                Close
	              </Button>
	              {isActive
	              	?
		              <Button variant="primary" type="submit">
		                Save Changes
		              </Button>
		              :
		              <Button variant="primary" disabled>
		                Save Changes
		              </Button>
		            }
	            </Modal.Footer>
	            </Form>
	          </Modal>
	        </>
		</>
	)
}