import { useEffect, useState, useContext } from 'react';
import { Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import CartTable from '../components/CartTable';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function CartView(){
	const {user} = useContext(UserContext);
	const [products, setProducts] =useState([])
	const [total, setTotal] =useState([])
	const history = useNavigate();

	useEffect(() => {
		fetch('https://shielded-sea-38438.herokuapp.com/users/getUserDetails', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// Total
			let sum = 0
			data.orders.forEach(order => sum += order.subTotal)
			setTotal(sum);
			// Product iteration for table
			setProducts(data.orders.map(product => {
				return (
					<CartTable key={product._id} productProp={product}/>
				)
			}))
		})
	}, [total]);

	const checkOut = () => {
		Swal.fire({
			title: `$${total} amount paid!`,
			icon: 'success',
			text: `Thank you for choosing The Guitar Shop!`
		});
		history("/");
	}
	
	return(
		(user.id !== null && user.isAdmin !== true)
		?
		<>
			<h1 className="styleH1Admin">Your Shopping Cart</h1>
			
			<Table striped bordered hover responsive size="sm">
			      <thead className="table-dark">
			        <tr>
			          <th>Name</th>
			          <th className="alignCenter">Price</th>
			          <th className="alignCenter">Quantity</th>
			          <th className="alignCenter">Subtotal</th>
			          <th className="alignCenter">Actions</th>
			        </tr>
			      </thead>
			      <tbody>
			      	{products}
			      	<tr>
			      		<td className=" alignCenter" colSpan="3">
			      			<Button className="chkBtn" onClick={() => checkOut()}>Checkout</Button>
			      		</td>
			      		<td className=" styleTotal alignCenter" colSpan="2">Total: ${total}</td>
			      	</tr>
			      </tbody>
		    </Table>
		</>
		:
		<Navigate to="/products"/>
	)
}