import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function Register(){

	const {user, setUser} = useContext(UserContext);

	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	// console.log(email);
	// console.log(password);

	useEffect(() => {
		if(firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password])

	function registerUser(e){
		e.preventDefault();

		fetch('https://shielded-sea-38438.herokuapp.com/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
			.then(res => res.json())
			.then(data => {
				// console.log(data)

				if(data){
					Swal.fire({
						title: 'Duplicate email found',
						icon: 'info',
						text: 'Please provide another email'
					})
				} else {
					fetch('https://shielded-sea-38438.herokuapp.com/users/userRegistration', {
						method: 'POST',
						headers: {
							'Content-Type' : 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password
						})
					})
					.then(res => res.json())
					.then(data => {
						// console.log(data)

						if(data.email){
							Swal.fire({
								title: 'Registration successful',
								icon: 'success',
								text: 'Thank you for registering'
							})

							history("/login")
							
						} else {
							Swal.fire({
								title: 'Registration failed',
								icon: 'error',
								text: 'Something went wrong'
							})
						}
					})
				}
			})

		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword('');
	};

	return (
		(user.id !== null)
		?
		<Navigate to="/products"/>
		:

		<div className="offset-md-3 col-md-6 styleRegister bgc2 fcwhite">
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label className="mt-2">First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your first name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label className="mt-2">Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your last name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label className="mt-2">Mobile Number:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your 11-digit mobile number here"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label className="mt-2">Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please enter your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label className="mt-2">Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please enter your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

		{ isActive
			?
			<Button variant="success" type="submit" id="submitBtn" className="mt-3">
				Register
			</Button>
			:
			<Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
				Register
			</Button>
		}
		</Form>
		</div>
	)
}